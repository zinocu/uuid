import 'package:test/test.dart';

import 'package:better_uuid/uuid.dart';

void main() {
  test('verifies empty guid', () {
    var id = Uuid.empty;
    expect(id.toByteList(), [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
    expect(() => Uuid.fromBytes([]), throwsArgumentError);
    expect(id.toString(), '00000000-0000-0000-0000-000000000000');
  });

  test('uuid from bytes', () {
    var id = Uuid.fromBytes([32, 151, 65, 3, 195, 188, 70, 18, 164, 78, 243, 206, 153, 208, 36, 86]);

    expect(id.timeLow, 0x20974103);
    expect(id.timeMid, 0xc3bc);
    expect(id.timeHigh, 0x612);
    expect(id.clockSeq, 0x244e);
    expect(id.clockSeqLow, 0x4e);
    expect(id.clockSeqHigh, 0x24);
    expect(id.node, 0xf3ce99d02456);
    expect(id.version, 4);
    expect(id.variant, UuidVariant.rfc4122);
    expect(id.toByteList(), [32, 151, 65, 3, 195, 188, 70, 18, 164, 78, 243, 206, 153, 208, 36, 86]);
    expect(id.toString(), '20974103-c3bc-4612-a44e-f3ce99d02456');
  });

  test('uuid from bytes with offset', () {
    var id = Uuid.fromBytes([0x1, 0x2, 91, 66, 139, 49, 153, 173, 84, 70, 146, 174, 73, 152, 143, 186, 169, 141], offset: 2);

    expect(id.version, 5);
    expect(id.variant, UuidVariant.rfc4122);
    expect(id.toByteList(), [91, 66, 139, 49, 153, 173, 84, 70, 146, 174, 73, 152, 143, 186, 169, 141]);
    expect(id.toString(), '5b428b31-99ad-5446-92ae-49988fbaa98d');
  });

  test('uuid from string v4', () {
    var id = Uuid('20974103-c3bc-4612-a44e-f3ce99d02456');

    expect(id.timeLow, 0x20974103);
    expect(id.timeMid, 0xc3bc);
    expect(id.timeHigh, 0x612);
    expect(id.clockSeq, 0x244e);
    expect(id.clockSeqLow, 0x4e);
    expect(id.clockSeqHigh, 0x24);
    expect(id.node, 0xf3ce99d02456);
    expect(id.version, 4);
    expect(id.variant, UuidVariant.rfc4122);
    expect(id.toByteList(), [32, 151, 65, 3, 195, 188, 70, 18, 164, 78, 243, 206, 153, 208, 36, 86]);
    expect(id.toString(), '20974103-c3bc-4612-a44e-f3ce99d02456');
  });

  test('uuid from string v1', () {
    var id = Uuid('c860fcd8-19ec-11e9-a9cd-6045cb34583b');

    expect(id.timeLow, 0xc860fcd8);
    expect(id.timeMid, 0x19ec);
    expect(id.timeHigh, 0x1e9);
    expect(id.clockSeq, 0x29cd);
    expect(id.clockSeqLow, 0xcd);
    expect(id.clockSeqHigh, 0x29);
    expect(id.node, 0x6045cb34583b);
    expect(id.version, 1);
    expect(id.variant, UuidVariant.rfc4122);
    expect(id.toByteList(), [200, 96, 252, 216, 25, 236, 17, 233, 169, 205, 96, 69, 203, 52, 88, 59]);
    expect(id.toString(), 'c860fcd8-19ec-11e9-a9cd-6045cb34583b');
  });

  test('uuid from various formats', () {
    var id1 = Uuid('{c860fcd8-19ec-11e9-a9cd-6045cb34583b}');
    var id2 = Uuid('{c860fcd819ec11e9a9cd6045cb34583b}');
    var id3 = Uuid('c860fcd819ec11e9a9cd6045cb34583b');
    var id4 = Uuid('(c860fcd8-19ec-11e9-a9cd-6045cb34583b)');
    var id5 = Uuid('(c860fcd819ec11e9a9cd6045cb34583b)');
    var id6 = Uuid('urn:uuid:c860fcd8-19ec-11e9-a9cd-6045cb34583b');
    var id7 = Uuid('urn:uuid:{c860fcd819ec11e9a9cd6045cb34583b}');

    expect(id1.toByteList(), [200, 96, 252, 216, 25, 236, 17, 233, 169, 205, 96, 69, 203, 52, 88, 59]);
    expect(id1.toString(), 'c860fcd8-19ec-11e9-a9cd-6045cb34583b');
    expect(id1 == id2, isTrue);
    expect(id2 == id3, isTrue);
    expect(id3 == id4, isTrue);
    expect(id4 == id5, isTrue);
    expect(id5 == id6, isTrue);
    expect(id6 == id7, isTrue);
    expect(id1 == id7, isTrue);
  });

  test('uuid string formatting', () {
    var id = Uuid('40e1217c-9cf7-47f9-b6c6-7046ec76e7be');

    expect(id.format(dashes: true, braces: true), '{40e1217c-9cf7-47f9-b6c6-7046ec76e7be}');
    expect(id.format(dashes: false, braces: true), '{40e1217c9cf747f9b6c67046ec76e7be}');
    expect(id.format(dashes: false, braces: false), '40e1217c9cf747f9b6c67046ec76e7be');
    expect(id.format(dashes: true, braces: false), '40e1217c-9cf7-47f9-b6c6-7046ec76e7be');
    expect(id.format(dashes: true, parens: true), '(40e1217c-9cf7-47f9-b6c6-7046ec76e7be)');
  });

  test('uuid equality and hashcode', () {
    var same1 = Uuid.fromBytes([195, 68, 112, 225, 75, 32, 73, 209, 145, 97, 172, 87, 233, 64, 202, 233]);
    var same2 = Uuid.fromBytes([195, 68, 112, 225, 75, 32, 73, 209, 145, 97, 172, 87, 233, 64, 202, 233]);
    var diff = Uuid.v4();

    expect(same1 == same2, isTrue);
    expect(same1.hashCode == same2.hashCode, isTrue);
    expect(same1.hashCode == diff.hashCode, isFalse);

    var map = <Uuid, String>{};

    map[same1] = 'Awesome sauce';
    map[diff] = 'Sour grapes';

    expect(map[same1], 'Awesome sauce');
    expect(map[same2], 'Awesome sauce');
    expect(map[diff], 'Sour grapes');

    map[same2] = 'Yolo squad';
    expect(map.length, 2);
    expect(map[same1], 'Yolo squad');
  });

  test('uuid v1', () {
    var id = Uuid.v1();
    var diff = Uuid.v1();

    expect(id.version, 1);
    expect(id.variant, UuidVariant.rfc4122);
    expect(id == diff, isFalse);
  });

  test('uuid v1 rapid creation', () {
    var ids = Set<Uuid>();
    var numIds = 5000;

    for (int i = 0; i < numIds; ++i) {
      ids.add(Uuid.v1());
    }

    expect(ids.length, numIds);
  });

  test('uuid v1 only timestamp', () {
    var timestamp = 8456;
    var id = Uuid.v1(timestamp: timestamp);

    expect(id.time, timestamp);
  });

  test('uuid v1 only clock sequence', () {
    var clockSequence = 8456;
    var id = Uuid.v1(clockSequence: clockSequence);

    expect(id.clockSeq, clockSequence);
  });

  test('uuid v1 only node', () {
    var node = 8456;
    var id = Uuid.v1(node: node);

    expect(id.node, node);
  });

  test('uuid v1 all parameters', () {
    var timestamp = 839523850982;
    var clockSequence = 2765;
    var node = 9875626547;
    var id = Uuid.v1(timestamp: timestamp, clockSequence: clockSequence, node: node, recreation: true);

    expect(id.time, timestamp);
    expect(id.clockSeq, clockSequence);
    expect(id.node, node);
  });

  test('uuid namespaces', () {
    var dnsId = Uuid.fromBytes(Uuid.namespaceDns);
    var urlId = Uuid.fromBytes(Uuid.namespaceUrl);
    var oidId = Uuid.fromBytes(Uuid.namespaceOid);
    var x500Id = Uuid.fromBytes(Uuid.namespaceX500);

    expect(dnsId.toString(), '6ba7b810-9dad-11d1-80b4-00c04fd430c8');
    expect(urlId.toString(), '6ba7b811-9dad-11d1-80b4-00c04fd430c8');
    expect(oidId.toString(), '6ba7b812-9dad-11d1-80b4-00c04fd430c8');
    expect(x500Id.toString(), '6ba7b814-9dad-11d1-80b4-00c04fd430c8');
  });

  test('uuid v3', () {
    var same1 = Uuid.v3(Uuid.namespaceDns, 'http://example.com');
    var same2 = Uuid.v3(Uuid.namespaceDns, 'http://example.com');
    var diff1 = Uuid.v3(Uuid.namespaceDns, 'http://example.net');
    var diff2 = Uuid.v3(Uuid.namespaceX500, 'http://example.com');

    expect(same1.version, 3);
    expect(same1.variant, UuidVariant.rfc4122);
    expect(same1 == same2, isTrue);
    expect(diff1 == same2, isFalse);
    expect(diff1 == diff2, isFalse);
  });

  test('uuid v4', () {
    Uuid id = Uuid.v4();
    Uuid diff = Uuid.v4();

    expect(id.version, 4);
    expect(id.variant, UuidVariant.rfc4122);
    expect(id == diff, isFalse);
  });

  test('uuid v5', () {
    var same1 = Uuid.v5(Uuid.namespaceOid, 'http://example.com');
    var same2 = Uuid.v5(Uuid.namespaceOid, 'http://example.com');
    var diff1 = Uuid.v5(Uuid.namespaceOid, 'http://example.net');
    var diff2 = Uuid.v5(Uuid.namespaceUrl, 'http://example.com');

    expect(same1.version, 5);
    expect(same1.variant, UuidVariant.rfc4122);
    expect(same1 == same2, isTrue);
    expect(diff1 != same2, isTrue);
    expect(diff1 == diff2, isFalse);
  });

  test('uuid comparison', () {
    var v1 = Uuid('9b693880-1b83-11e9-a5b9-6045cb34583b');
    var v1diff = Uuid('9b693870-1b83-11e9-a5b9-6045cb34583b');
    var v3 = Uuid.v3(Uuid.namespaceDns, 'http://example.com');
    var v4 = Uuid('ab3dd141-e2b3-41fa-bfba-2e1bc3b0adac');
    var v4same = Uuid('ab3dd141-e2b3-41fa-bfba-2e1bc3b0adac');
    var v5 = Uuid.v5(Uuid.namespaceUrl, 'http://example.com');

    expect(v1.compareTo(v4) < 0, isTrue);
    expect(v5.compareTo(v3) > 0, isTrue);
    expect(v3.compareTo(v1) > 0, isTrue);
    expect(v4.compareTo(v5) < 0, isTrue);
    expect(v4.compareTo(v4same) == 0, isTrue);
    expect(v1.compareTo(v1diff) > 0, isTrue);
  });

  test('uuid to big integer', () {
    var id1 = Uuid('034241fa-f561-4410-8609-7b7afa215c6e');
    var id2 = Uuid('5ed71166-adcd-4faf-915c-8716094f92fd');
    
    expect(id1.toBigInt(), BigInt.parse('4331713819624326803315676722621406318'));
    expect(id2.toBigInt(), BigInt.parse('126064128364392031324172012855306982141'));
  });

  test('uuid from big integer', () {
    var n = BigInt.parse('126064128364392031324172012855306982141');
    var id = Uuid(n.toRadixString(16));

    expect(id.toBigInt(), n);
  });
}
