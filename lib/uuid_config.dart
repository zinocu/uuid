library uuid;

import 'dart:math';

var _secureRandom = Random.secure();

/// Configuration for version 1 UUIDs
class UuidConfig {
  static final DateTime _epoch = DateTime.utc(1582, 10, 15);
  static const int maxTimestampChanges = 9;
  static int numUuidsInTimestamp = 0;
  static int lastClockSequence;
  static int lastTimestamp;
  static int nodeId;
  static bool initialized = false;
  static int get currentTimestamp =>
      DateTime.now().toUtc().difference(_epoch).inMicroseconds * 10;

  static void generate() {
    // RFC4122 Section 4.1.5; generate a random 14-bit clock sequence
    lastClockSequence = _secureRandom.nextInt(1 << 14) & 0x3fff;

    // RFC4122 Section 4.1.2; timestamps are defined as the count of 100-nanosecond
    // intervals since the Gregorian calender epoch of 1582-10-15 00:00
    var duration = DateTime.now().toUtc().difference(_epoch);
    lastTimestamp = duration.inMicroseconds * 10;

    // RFC4122 Section 4.1.6; generate a 48-bit node id (47 random bits + 1 multi-cast bit)
    nodeId = (_secureRandom.nextInt(1 << 32) << 16) |
        (_secureRandom.nextInt(1 << 16) | 0x01);

    initialized = true;
  }
}
