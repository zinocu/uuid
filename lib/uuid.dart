library uuid;

import 'dart:math';
import 'dart:core';
import 'dart:typed_data';

import 'package:crypto/crypto.dart';
import 'package:crypto/src/digest_sink.dart';

import 'uuid_config.dart';

var _secureRandom = Random.secure();
var _uuidReplaceReg = RegExp(r'[\{\}\(\)\-]');

/// The possible variants for a UUID
enum UuidVariant {
  ncs, rfc4122, microsoft, future
}

/// A class that represents a UUID object whose values cannot be changed after creation
class Uuid implements Comparable<Uuid> {
  /// A Uuid object to represent a null UUID
  static final Uuid empty = Uuid.fromBytes([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
  /// The number of bytes that a UUID must contain
  static const int uuidByteLength = 16;
  // Namespaces given in RFC 4122 for version 3 and 5 UUIDs
  static const namespaceDns = [0x6b, 0xa7, 0xb8, 0x10, 0x9d, 0xad, 0x11, 0xd1, 0x80, 0xb4, 0x0, 0xc0, 0x4f, 0xd4, 0x30, 0xc8];
  static const namespaceUrl = [0x6b, 0xa7, 0xb8, 0x11, 0x9d, 0xad, 0x11, 0xd1, 0x80, 0xb4, 0x0, 0xc0, 0x4f, 0xd4, 0x30, 0xc8];
  static const namespaceOid = [0x6b, 0xa7, 0xb8, 0x12, 0x9d, 0xad, 0x11, 0xd1, 0x80, 0xb4, 0x0, 0xc0, 0x4f, 0xd4, 0x30, 0xc8];
  static const namespaceX500 = [0x6b, 0xa7, 0xb8, 0x14, 0x9d, 0xad, 0x11, 0xd1, 0x80, 0xb4, 0x0, 0xc0, 0x4f, 0xd4, 0x30, 0xc8];

  int _timeLow;
  int _timeMid;
  int _timeHighAndVersion;
  int _clockSeqHighAndVariant;
  int _clockSeqLow;
  int _node;

  /// Create a UUID from a string
  ///
  /// The [uuid] can be in many formats:
  /// 5b428b31-99ad-5446-92ae-49988fbaa98d
  /// {5b428b31-99ad-5446-92ae-49988fbaa98d}
  /// 5b428b3199ad544692ae49988fbaa98d
  /// {5b428b3199ad544692ae49988fbaa98d}
  /// urn:uuid:5b428b31-99ad-5446-92ae-49988fbaa98d
  /// urn:uuid:{5b428b3199ad544692ae49988fbaa98d}
  /// (5b428b31-99ad-5446-92ae-49988fbaa98d)
  /// and a few more variations
  Uuid(String uuid) {
    var id = uuid.replaceAll(_uuidReplaceReg, '').replaceAll('urn:uuid:', '');
    if (id.length != 32) {
      throw ArgumentError.value(uuid, 'uuid', 'UUID must contain 32 hex characters');
    }

    var arr = Uint8List(uuidByteLength);
    for (int i = 0; i < uuidByteLength; ++i) {
      arr[i] = (int.parse(id.substring(i * 2, (i + 1) * 2), radix: 16));
    }

    _setBytes(arr);
  }
  
  /// Create a UUID from a byte list
  ///
  /// The [bytes] list must have at least 16 items
  /// If [offset] is given, the bytes for the uuid will be retrieved starting at the offset
  Uuid.fromBytes(List<int> bytes, {int offset = 0}) {
    var arr = bytes.skip(offset).take(uuidByteLength).toList();
    if (arr.length != uuidByteLength) {
      throw ArgumentError('UUID must have exactly $uuidByteLength bytes, not ${arr.length}');
    }

    _setBytes(arr);
  }

  /// Set the private variables for the UUID from the given [bytes].
  _setBytes(List<int> bytes) {
    _timeLow = ((bytes[0] & 0xff) << 24)
    | ((bytes[1] & 0xff) << 16)
    | ((bytes[2] & 0xff) << 8)
    | (bytes[3] & 0xff);
    _timeMid = ((bytes[4] & 0xff) << 8)
    | (bytes[5] & 0xff);
    _timeHighAndVersion = ((bytes[6] & 0xff) << 8)
    | (bytes[7] & 0xff);
    _clockSeqHighAndVariant = (bytes[8] & 0xff);
    _clockSeqLow = (bytes[9] & 0xff);
    _node = ((bytes[10] & 0xff) << 40)
    | ((bytes[11] & 0xff) << 32)
    | ((bytes[12] & 0xff) << 24)
    | ((bytes[13] & 0xff) << 16)
    | ((bytes[14] & 0xff) << 8)
    | (bytes[15] & 0xff);
  }

  /// Create a version 1 UUID
  ///
  /// If any of [timestamp], [clockSequence], or [node] are given, they will be used
  /// instead of getting them from [UuidConfig]
  /// If [timestamp], [clockSequence], and [node] are all given, [recreation] should also be set to
  /// `true`. If not, clockSequence will be increased if [timestamp] is less than the current timestamp.
  ///
  /// The purpose of the [recreation] boolean is to allow creation of past UUIDs without
  /// checking or modifying [UuidConfig]
  Uuid.v1({int timestamp, int clockSequence, int node, bool recreation = false}) {
    if (!UuidConfig.initialized) {
      UuidConfig.generate();
    }

    timestamp = timestamp ?? UuidConfig.currentTimestamp;
    clockSequence = clockSequence ?? UuidConfig.lastClockSequence;
    node = node ?? UuidConfig.nodeId;

    if (!recreation) {
      if (timestamp == UuidConfig.lastTimestamp) {
        // We are still on the last timestamp, so determine how to prevent a collision
        if (UuidConfig.numUuidsInTimestamp > UuidConfig.maxTimestampChanges) {
          // The timestamp for this interval has been changed the max number of times,
          // so increment the clock sequence to avoid a collision
          clockSequence = (clockSequence + 1) & 0x3fff;
        }
        else {
          // The very first uuid in a timestamp will mean that the timestamp is incremented by 0
          // and the last uuid will result in a timestamp incremented by 9
          timestamp += UuidConfig.numUuidsInTimestamp;
        }

        // add 1 to the num uuids for the one about to be created
        UuidConfig.numUuidsInTimestamp += 1;
      }
      else if (timestamp < UuidConfig.lastTimestamp) {
        // If, for some reason, the timestamp is lower than the previous timestamp
        // despite the fact that this is not a uuid recreation, increment the clock sequence
        clockSequence = (clockSequence + 1) & 0x3fff;
      }
      else {
        // If this uuid is requested further in time than the last v1 uuid,
        // make sure the num uuids in the timestamp is 1 because it is about to be created.
        // Now we can set the last timestamp to the new time for the next time
        // a v1 uuid is requested
        UuidConfig.numUuidsInTimestamp = 1;
        UuidConfig.lastTimestamp = timestamp;
      }

      UuidConfig.lastClockSequence = clockSequence;
    }

    _timeLow = (timestamp & 0xffffffff);
    _timeMid = (timestamp & 0xffff00000000) >> 32;
    _timeHighAndVersion = (timestamp & 0xfff000000000000) >> 48;
    _timeHighAndVersion = 0x1000 | (_timeHighAndVersion & 0xfff); // set version to 1
    _clockSeqHighAndVariant = ((clockSequence & 0x3f00) >> 8) | 0x80; // set variant to RFC 4122
    _clockSeqLow = clockSequence & 0xff;
    _node = node & 0xffffffffffff;
  }

  /// Create a version 3 UUID
  ///
  /// The [namespace] should be one of [Uuid.namespaceDns], [Uuid.namespaceUrl],
  /// [Uuid.namespaceOid], or [Uuid.namespaceX500]. However, it can be any list of bytes.
  /// The [name] is concatenated to [namespace] and hashed with MD5
  Uuid.v3(List<int> namespace, String name) {
    var ds = DigestSink();
    var s = md5.startChunkedConversion(ds);
    s.add(namespace);
    s.add(name.codeUnits);
    s.close();

    var hash = ds.value.bytes;

    _setBytes(hash);

    _timeHighAndVersion = 0x3000 | (_timeHighAndVersion & 0xfff); // set version to 3
    _clockSeqHighAndVariant = (_clockSeqHighAndVariant & 0x3f) | 0x80; // set variant to RFC 4122
  }

  /// Create a version 4 UUID
  ///
  /// Random values are chosen for the member variables of a version 4 UUID.
  /// If [random] is specified, the random numbers will be generated from it.
  /// If not, a secure random generator will be used.
  Uuid.v4([Random random]) {
    var rand = random ?? _secureRandom;
    _timeLow = rand.nextInt(1 << 32);
    _timeMid = rand.nextInt(1 << 16);
    _timeHighAndVersion = rand.nextInt(1 << 16);
    _timeHighAndVersion = 0x4000 | (_timeHighAndVersion & 0xfff); // set version to 4
    _clockSeqHighAndVariant = rand.nextInt(1 << 8);
    _clockSeqHighAndVariant = (_clockSeqHighAndVariant & 0x3f) | 0x80; // set variant to RFC 4122
    _clockSeqLow = rand.nextInt(1 << 8);
    _node = rand.nextInt(1 << 32) << 16;
    _node |= rand.nextInt(1 << 16);
  }

  /// Create a version 5 UUID
  ///
  /// The [namespace] should be one of [Uuid.namespaceDns], [Uuid.namespaceUrl],
  /// [Uuid.namespaceOid], or [Uuid.namespaceX500]. However, it can be any list of bytes.
  /// The [name] is concatenated to [namespace] and hashed with SHA1
  Uuid.v5(List<int> namespace, String name) {
    var ds = DigestSink();
    var s = sha1.startChunkedConversion(ds);
    s.add(namespace);
    s.add(name.codeUnits);
    s.close();

    var hash = ds.value.bytes;

    _setBytes(hash);

    _timeHighAndVersion = 0x5000 | (_timeHighAndVersion & 0xfff); // set version to 5
    _clockSeqHighAndVariant = (_clockSeqHighAndVariant & 0x3f) | 0x80; // set variant to RFC 4122
  }

  int get timeLow => _timeLow;

  int get timeMid => _timeMid;

  int get timeHigh => _timeHighAndVersion & 0xfff;

  int get time => (timeHigh << 48) | (timeMid << 32) | timeLow;

  int get timeHighAndVersion => _timeHighAndVersion;

  int get version => (_timeHighAndVersion & 0xf000) >> 12;

  int get clockSeq => (clockSeqHigh << 8) | _clockSeqLow;

  int get clockSeqLow => _clockSeqLow;

  int get clockSeqHigh => _clockSeqHighAndVariant & 0x3f;

  int get clockSeqHighAndVariant => _clockSeqHighAndVariant;

  int get node => _node;

  UuidVariant get variant {
    int n = (_clockSeqHighAndVariant & 0xe0) >> 5;
    switch (n) {
      case 0:
      case 1:
      case 2:
      case 3:
        return UuidVariant.ncs;
      case 4:
      case 5:
        return UuidVariant.rfc4122;
      case 6:
        return UuidVariant.microsoft;
      case 7:
        return UuidVariant.future;
    }

    throw StateError('Unknown variant. This should never occur');
  }

  /// Convert the UUID to a list of bytes
  Uint8List toByteList() {
    var bytes = Uint8List(uuidByteLength);

    bytes[0] = (_timeLow & 0xff000000) >> 24;
    bytes[1] = (_timeLow & 0xff0000) >> 16;
    bytes[2] = (_timeLow & 0xff00) >> 8;
    bytes[3] = (_timeLow & 0xff);
    bytes[4] = (_timeMid & 0xff00) >> 8;
    bytes[5] = (_timeMid & 0xff);
    bytes[6] = (_timeHighAndVersion & 0xff00) >> 8;
    bytes[7] = (_timeHighAndVersion & 0xff);
    bytes[8] = (_clockSeqHighAndVariant & 0xff);
    bytes[9] = (_clockSeqLow & 0xff);
    bytes[10] = (_node & 0xff0000000000) >> 40;
    bytes[11] = (_node & 0xff00000000) >> 32;
    bytes[12] = (_node & 0xff000000) >> 24;
    bytes[13] = (_node & 0xff0000) >> 16;
    bytes[14] = (_node & 0xff00) >> 8;
    bytes[15] = (_node & 0xff);

    return bytes;
  }

  /// Convert the UUID to a BigInt
  BigInt toBigInt() {
    return BigInt.parse(format(dashes: false), radix: 16);
  }

  @override
  String toString() {
    return format(dashes: true, braces: false);
  }

  /// Convert the UUID to a string representation
  ///
  /// If [dashes] is true, dashes will be used to separate parts of the UUID hex
  /// If [braces] is true, the resulting string will be enclosed in braces: { }
  /// If [parens] is true, the resulting string will be enclosed in parentheses: ( )
  String format({bool dashes = true, bool braces = false, bool parens = false}) {
    var list = List<String>.filled(12, '');
    list[1] = _timeLow.toRadixString(16).padLeft(8, '0');
    list[3] = _timeMid.toRadixString(16).padLeft(4, '0');
    list[5] = _timeHighAndVersion.toRadixString(16).padLeft(4, '0');
    list[7] = _clockSeqHighAndVariant.toRadixString(16).padLeft(2, '0');
    list[8] = _clockSeqLow.toRadixString(16).padLeft(2, '0');
    list[10] = _node.toRadixString(16).padLeft(12, '0');

    if (braces) {
      list[0] = '{';
      list[11] = '}';
    }
    else if (parens) {
      list[0] = '(';
      list[11] = ')';
    }

    if (dashes) {
      list[2] = '-';
      list[4] = '-';
      list[6] = '-';
      list[9] = '-';
    }

    return list.join();
  }

  @override
  int get hashCode => (_timeLow << 16) ^ _timeMid ^ (_timeHighAndVersion << 32)
                    ^ (_clockSeqHighAndVariant << 8) ^ _clockSeqLow ^ _node;

  @override
  bool operator ==(other) {
    if (other == null || other is! Uuid) {
      return false;
    }

    var id = other as Uuid;

    return this._timeLow == id._timeLow
        && this._timeMid == id._timeMid
        && this._timeHighAndVersion == id._timeHighAndVersion
        && this._clockSeqHighAndVariant == id._clockSeqHighAndVariant
        && this._clockSeqLow == id._clockSeqLow
        && this._node == id._node;
  }

  @override
  int compareTo(Uuid other) {
    // compare version first
    int diff = version - other.version;
    if (diff != 0) {
      return diff;
    }

    diff = _timeLow - other._timeLow;
    if (diff != 0) {
      return diff;
    }

    diff = _timeMid - other._timeMid;
    if (diff != 0) {
      return diff;
    }

    diff = _timeHighAndVersion - other._timeHighAndVersion;
    if (diff != 0) {
      return diff;
    }

    diff = _clockSeqLow - other._clockSeqLow;
    if (diff != 0) {
      return diff;
    }

    diff = _clockSeqHighAndVariant - other._clockSeqHighAndVariant;
    if (diff != 0) {
      return diff;
    }

    diff = _node - other._node;
    if (diff != 0) {
      return diff;
    }

    return 0;
  }
}