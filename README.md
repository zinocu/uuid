# better_uuid

A better uuid package for the dart language

## Getting Started

Include better_uuid in your project by editing the dependencies of your dart project's pubspec.yaml

```
dependencies:

  better_uuid: ^0.1.2
```

## Usage
### Version 1 UUID
```dart
import 'package:better_uuid/uuid.dart';

var id = Uuid.v1(); // Uuid object
print(id.time); // 137671473757059390
print(id.version); // 1
print(id.variant); // UuidVariant.rfc4122
print(id.clockSeq); // 12781
print(id.node); // 56765789678
```
**Note:** Dart does not currently support getting the MAC address of a device in an easy way, so the node variable for version 1 UUIDs is randomly generated the very first time a version 1 UUID is requested, and is kept consistent throughout the life of the program.

Parameters can be passed to version 1 UUIDs if you want the same values used in multiple UUIDs

#### Version 1 with consistent node (perhaps to ensure the same node value in different program lives):
```dart
node = 67985654;
var id1 = Uuid.v1(node: node);
print(id1.toString()); // e34ae8a2-1b86-11e9-a108-0000040d60f6
var id2 = Uuid.v1(node: node);
print(id2.toString()); // e34b0fc6-1b86-11e9-a108-0000040d60f6
```

#### Version 1 Recreation
```dart
var timestamp = 839523850982;
var clockSequence = 2765;
var node = 9875626547;
var id = Uuid.v1(timestamp: timestamp, clockSequence: clockSequence, node: node, recreation: true);
```

If all three numeric parameters, `timestamp`, `clockSequence`, and `node` are given, you should also specify `recreation: true`
If `recreation` is left as false and the timestamp is less than the current timestamp in `UuidConfig.currentTimestamp`, the clock sequence will be incremented and the UUID will be different.

### Version 3 and 5 UUID

Version 3 and 5 UUIDs are made by taking in 2 required paramters:
* `namespace` -- List<int> of the namespace bytes
* `name` -- String
```dart
var same1 = Uuid.v3(Uuid.namespaceDns, 'http://example.com');
var same2 = Uuid.v3(Uuid.namespaceDns, 'http://example.com');

assert(same1 == same2);
```

```dart
var same1 = Uuid.v5(Uuid.namespaceOid, 'http://test.com');
var same2 = Uuid.v5(Uuid.namespaceOid, 'http://test.com');

assert(same1 == same2);
```
**Default Namespaces Given by RFC 4122:** `Uuid.namespaceDns`, `Uuid.namespaceUrl`, `Uuid.namespaceOid`, `Uuid.namespaceX500`
Any byte list can be used for the namespace parameter, not just the ones listed above.

### Version 4 UUID

All variables of the version 4 UUID are randomly generated with `Random.secure()`
```dart
Uuid id = Uuid.v4();
Uuid diff = Uuid.v4();

print(id.version); // 4
assert(id != diff);
```

**Version 4 UUIDs are typically preferred over any other version**

## String conversion
#### From String to Uuid
UUIDs can be created from a given string in any number of formats:
```dart
var id1 = Uuid('c860fcd8-19ec-11e9-a9cd-6045cb34583b');
var id2 = Uuid('{c860fcd8-19ec-11e9-a9cd-6045cb34583b}');
var id3 = Uuid('{c860fcd819ec11e9a9cd6045cb34583b}');
var id4 = Uuid('c860fcd819ec11e9a9cd6045cb34583b');
var id5 = Uuid('urn:uuid:40e1217c-9cf7-47f9-b6c6-7046ec76e7be');
```

#### From Uuid to String
```dart
var id = Uuid('c860fcd8-19ec-11e9-a9cd-6045cb34583b');
print(id.toString()); // c860fcd8-19ec-11e9-a9cd-6045cb34583b
print(id.format(braces: true, dashes: false)); // {c860fcd819ec11e9a9cd6045cb34583b}
```

The `toString()` of a Uuid object simply calls `format(dashes: true, braces: false)` which gives a string in the canonical format

## Byte Conversion
#### From bytes to Uuid
```dart
var id = Uuid.fromBytes([32, 151, 65, 3, 195, 188, 70, 18, 164, 78, 243, 206, 153, 208, 36, 86]);

print(id); // 20974103-c3bc-4612-a44e-f3ce99d02456
print(id.version); // 4
print(id.variant); //UuidVariant.rfc4122
```

An optional parameter `offset` can be passed to `Uuid.fromBytes` in order to skip a number of bytes in the list before beginning parsing the bytes

```dart
var id = Uuid.fromBytes([0xff, 0xff, 32, 151, 65, 3, 195, 188, 70, 18, 164, 78, 243, 206, 153, 208, 36, 86], offset: 2);
```

## Map and Set
Uuid objects can be used in `Set` and `Map` keys
```dart
var same1 = Uuid.fromBytes([195, 68, 112, 225, 75, 32, 73, 209, 145, 97, 172, 87, 233, 64, 202, 233]);
var same2 = Uuid.fromBytes([195, 68, 112, 225, 75, 32, 73, 209, 145, 97, 172, 87, 233, 64, 202, 233]);
var diff = Uuid.v4();

var map = <Uuid, String>{}; // map with Uuid as key
map[same1] = 'Awesome sauce';
map[diff] = 'Sour grapes';

// a different Uuid object, but the same value
print(map[same2]); // Awesome sauce
```

#### More detailed examples are in the test folder