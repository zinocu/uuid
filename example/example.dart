import 'package:better_uuid/uuid.dart';

class User {
  Uuid id;
  String username;

  User(this.id, this.username);

  User.create(this.username) {
    id = Uuid.v4();
  }
}

void main() {
  var john = User.create("johndoe");
  var james = User.create("james");

  assert(john.id != james.id);
}